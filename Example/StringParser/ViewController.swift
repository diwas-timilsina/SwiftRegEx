//
//  ViewController.swift
//  StringParser
//
//  Created by Diwas  Timilsina on 7/19/15.
//  Copyright (c) 2015 Diwas  Timilsina. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // split example
        var regEx = SwiftRegEx(initWithPattern: "\\w+")
        var arr: [String] = regEx!.split("Hello World")
        println(arr)
        
        // replace example
        regEx = SwiftRegEx(initWithPattern: "hello")
        println(regEx!.replace("hello hello world", with: "People"))
        
        //get Index example
        regEx = SwiftRegEx(initWithPattern: "hel+")
        println(regEx!.getIndex("a hello world globe people "))
        
    }
}

