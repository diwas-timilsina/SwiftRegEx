<a name="introduction"/>
## Introduction

This project is a wrapper class to the regular expressions in Swift. 
As of iOS 4, NSRegularExpression is build in to Framework libarary. However, the libarary is 
not very user friendly. SwiftRegEx provides a user firendly wrapper class to NSRegularExpression. 

Here is an example of how to use the wrapper libarary:

```swift
//create SwiftRegEx 
var regEx = SwiftRegEx(initWithPattern: "//w+")
//returns ["Hello", "World"]
regEx.split("Hello World")
```


## Installing 
This libarary has no dependencies and works for iOS 4.0 + and OSX 10.7+. All you need to do is just copy the following file into your project:
 - [SwiftRegEx.swift] (https://gitlab.com/diwas-timilsina/SwiftRegEx/tree/master/SwiftRegEx.swift)
 
## Quick Example
Here are some short examples of how to use the library. 
```swift
\\creating swiftRegEx object
var regEx = SwiftRegEx(initWithPattern: "//w+")
var regEx = SwiftRegEX(initWithPattern: "//w+", caseSenative: true)
var regEx = SwiftRegEx(initWithPattern: "//w+", options: NSRegularExpressionOptions.IgnoreMetacharacters)

\\check for a given string matches the regular expression
\\ the value of matchFound will be True
var regEx = SwiftRegEx(initWithPattern: "he+")
let matchFound: Bool = regEx.isMatch("hello world")

\\get index of the match in the given string
\\ the value of index will be 6
var regEx = SwiftRegEx(initWithPattern: "hello world")
let index = regEx.getIndex("world")

\\split the given string based on the regex 
\\ splitString = ["Hello", "World"]
var regEx = SwiftRegEx(initWithPattern: "\\w+")
let splitString = regEx.split("Hello world") as [String]

\\replace string with another string 
\\ replaceString will have value "organge orange ball cat"
var regEx = SwiftRegEx(initWithPattern: "apple")
let replacedString = regEx.replace("apple apple ball cat", "orange")

\\replace string by passing in a block
\\ replacedString will have value "orange orange ball cat"
var regEx = SwiftRegEx(initWithPattern: "apple")
let replacedString = regEx.replace("apple apple ball cat", replacer: { (inputString) -> String in
    return "orange"
})

\\return the first match in the string
\\ match = "World" 
var regEX = SwiftRegEx(initWithPattern: "w+")
let matches = regEx.firstMatch("Hello World, Welcome!")

\\return an array of matched strings
\\ matches = ["World", "Welcome!"]
var regEX = SwiftRegEx(initWithPattern: "w+")
let matches = regEx.matches("Hello World, Welcome!")

```
 
## Licensing 

The MIT License (MIT)

Copyright (c) 2015 Diwas Timilsina

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.